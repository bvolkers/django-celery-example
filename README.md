Run django:
```shell
cd backend && python3 manage.py runserver
```

Run celery:
```shell
cd backend && celery -A data_science worker -l info
```
Or with limited concurrency:
```shell
cd backend && celery -A data_science worker -c 1 -l info
```

Run test client:
```shell
python3 test_django_client.py
```