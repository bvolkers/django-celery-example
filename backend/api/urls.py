from django.urls import path

from api.views import AnalysisView, ResultView, AnalysisResultView, ExecuteView

urlpatterns = [
    path('api/analysis/', AnalysisView.as_view()),
    path('api/result/', ResultView.as_view()),
    path('api/analysis-result/', AnalysisResultView.as_view()),
    path('api/execute/', ExecuteView.as_view()),
]
