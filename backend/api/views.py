import time

import numpy as np
from celery.result import AsyncResult
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.request import Request
from data_science.tasks import mul, predict_single


class AnalysisView(generics.views.APIView):
    def post(self, req: Request):
        task_id = predict_single.delay(req.data['data'])
        return Response({'task_id': str(task_id), 'status': 'Processing'}, status=202)


class ResultView(generics.views.APIView):
    def post(self, req: Request):
        """Fetch result for given task_id"""
        task_id = req.data['task_id']
        task = AsyncResult(task_id)
        if not task.ready():
            return Response({'task_id': str(task_id), 'status': 'Processing'}, status=202)
        result = task.get()
        return Response({'task_id': task_id, 'status': 'Success', 'value': str(result)})


class AnalysisResultView(generics.views.APIView):
    def post(self, req: Request):
        task = predict_single.delay(req.data['data'])
        result = task.get()
        return Response({'task_id': str(task), 'status': 'Success', 'value': str(result)})


class ExecuteView(generics.views.APIView):
    """Example for multiple tasks and cancelling"""

    @staticmethod
    def cancel_all(tasks):
        for task in tasks:
            task.revoke()

    def post(self, req: Request):
        inputs = req.data['data']
        tasks = []
        for input_row in inputs:
            task = predict_single.delay(input_row)
            tasks.append(task)

        # Busy waiting
        results = [None] * len(tasks)
        results_ready = [False] * len(tasks)
        while True:
            for i, task in enumerate(tasks):
                if task.ready():
                    results[i] = task.get()
                    results_ready[i] = True
                    if task.result["is_defect"]:
                        return Response({'status': 'Failure', 'value': str(results)})
            if all(results_ready):
                break
            time.sleep(0.1)

        return Response({'status': 'Success', 'value': str(results)})

