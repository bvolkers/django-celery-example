import torch.nn as nn


class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Linear(2, 1)

    def init_weights(self):
        self.model.weight.data.normal_(mean=0, std=1)

    def forward(self, x):
        return self.model.forward(x)