import logging

import joblib
import os

import numpy as np
import pandas as pd
import torch
from dataclasses import dataclass

from .net import Net

MODEL_PATH = "data_science/model.pt"


@dataclass
class Result:
    value: float
    is_defect: bool


class ModelWrapper:
    """ Wrapper for loading and serving pre-trained model"""

    def __init__(self, path=MODEL_PATH, gpu=True):
        torch.autograd.set_grad_enabled(False)
        self.device = 'cuda' if torch.cuda.is_available() and gpu else 'cpu'
        self.model = Net().to(self.device)
        self.model.load_state_dict(torch.load(path))
        self.model.eval()

    def predict(self, data):
        logging.info(f"Input: {data}")
        if isinstance(data, list):
            data = torch.tensor(data, dtype=torch.float, device=self.device)
        if isinstance(data, np.ndarray):
            data = torch.from_numpy(data).float().to(self.device)
        prediction = self.model(data)
        logging.info(f"Prediction: {prediction}")
        return Result(value=prediction.item(), is_defect=True).__dict__

