import torch
import torch.nn as nn
from data_science.ml.net import Net

torch.manual_seed(10)
model = Net()
model.init_weights()

r = model(torch.Tensor([1, 0]))
print(r)

torch.save(model.state_dict(), "linear1.pt")