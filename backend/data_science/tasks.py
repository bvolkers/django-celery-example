import importlib
import logging
import time

from celery import Task
from celery import shared_task


class PredictTask(Task):
    """
    Abstraction of Celery's Task class to support loading ML model.

    """
    abstract = True

    def __init__(self):
        super().__init__()
        self.model = None

    def __call__(self, *args, **kwargs):
        """
        Load model on first call (i.e. first task processed)
        Avoids the need to load model on each task request
        """
        if not self.model:
            logging.info('Loading Model...')
            module_import = importlib.import_module(self.path[0])
            model_obj = getattr(module_import, self.path[1])
            self.model = model_obj()
            logging.info('Model loaded')
        return self.run(*args, **kwargs)


@shared_task(ignore_result=False,
             bind=True,
             base=PredictTask,
             path=('data_science.ml', 'ModelWrapper'))
def predict_single(self, data):
    """
    Essentially the run method of PredictTask
    """
    pred = self.model.predict(data)
    return pred


@shared_task
def mul(data):
    a, b = data
    time.sleep(5)
    return a * b
