import requests
from time import sleep
import logging

test_body = {
    "data": [10, 20]
}

test_execute_body = {
    "data": [[10, 20], [10, 30]]
}


def dummy_django(data, poll_interval=2, max_attempts=5):
    base_uri = r'http://127.0.0.1:8000'
    predict_task_uri = base_uri + '/api/analysis/'
    task = requests.post(predict_task_uri, json=data)
    task_id = task.json()['task_id']
    predict_result_uri = base_uri + '/api/result/'
    attempts = 0
    result = None
    while attempts < max_attempts:
        logging.info("Attempting...")
        attempts += 1
        result_response = requests.post(predict_result_uri, json={'task_id': task_id})
        if result_response.status_code == 200:
            result = result_response.json()['value']
            break
        sleep(poll_interval)
    return result


def dummy_django_analysis_result(data):
    base_uri = r'http://127.0.0.1:8000'
    predict_task_uri = base_uri + '/api/analysis-result/'
    response = requests.post(predict_task_uri, json=data)
    result = response.json()['value']
    return result


def dummy_django_execute(data):
    base_uri = r'http://127.0.0.1:8000'
    predict_task_uri = base_uri + '/api/execute/'
    response = requests.post(predict_task_uri, json=data)
    result = response.json()['value']
    return result


if __name__ == '__main__':
    # result = dummy_django(test_body)
    result = dummy_django_analysis_result(test_body)
    # result = dummy_django_execute(test_execute_body)
    print(f"{result=}")
